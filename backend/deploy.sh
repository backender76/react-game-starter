#!/usr/bin/env bash

# Деплой на продакшен сервер

RED="\033[0;31m"
NC="\033[0m" # No Color

if [ "$1" == "--force" ]; then
  FORCE=true
else
  FORCE=false
fi

# Переход в папку backend (на случай если запускается из другой папки)
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd "$DIR"

# Чтение конфига
if [ -f "./config.sh" ]; then
  source "./config.sh"
else
  source "../config.sh"
fi

echo "Начало выгрузки backend-сборки на сервер $SSH_HOST"
echo "Пользователь SSH: $SSH_USER"

# Проверка текущей ветки
GIT_BRANCH_NAME=$(git rev-parse --abbrev-ref HEAD)

if [ "$GIT_BRANCH_NAME" != "master" ]; then
  echo -e "${RED}Отмена. Деплой допускается только из ветки master. Текущая ветка: ${GIT_BRANCH_NAME}${NC}"
  exit
fi

# Проверка наличия изменений
GIT_DIFF=$(git diff)

if [ "$GIT_DIFF" != "" ]; then
  echo -e "${RED}Отмена. Имеются не сохранённые изменения в рабочем каталоге.${NC}"
  exit
fi

# Получение имени
PACKAGE_NAME=$(node <<EOF
try {
  const package = require('./package.json');
  console.log(package.name);
} catch (ignored) {}
EOF
)
echo "PACKAGE_NAME: $PACKAGE_NAME"

# Получение номера порта из production-конфига
if [ ! -f "./config/production.json" ]; then
  echo "Отмена. Не создан продакшен конфиг (./config/production.json)"
  exit
fi
WEB_SERVER_PORT=$(node <<EOF
try {
  const config = require('./config/production.json');
  console.log(config.webServer.port);
} catch (err) {
  console.log(err.message);
}
EOF
)
if [[ ! "$WEB_SERVER_PORT" =~ ^[0-9]+$ ]]; then
    echo "C \"config.webServer.port\" что-то не так: $WEB_SERVER_PORT"
    echo "Проверьте синтаксис продакшен конфига."
    exit
fi
echo "WEB_SERVER_PORT: $WEB_SERVER_PORT"

# Имя архива и сервиса
SERVICE_NAME="$PACKAGE_NAME-$WEB_SERVER_PORT"
ARCHIVE_NAME="$SERVICE_NAME.zip"
echo "SERVICE_NAME: $SERVICE_NAME"
echo "ARCHIVE_NAME: $ARCHIVE_NAME"

# Проверка не занят ли порт

# a (смотрим в папке services)
SIMILAR_SERVICES=$(ssh -p "$SSH_PORT" "$SSH_USER@$SSH_HOST" ls services | grep "\-$WEB_SERVER_PORT")
echo "Похожие сервисы: $SIMILAR_SERVICES"

OTHER_SERVICES=$(echo "$SIMILAR_SERVICES" | grep -v "$SERVICE_NAME")
echo "Похожие сервисы включающие имя текущего: $OTHER_SERVICES"

if [ "$SIMILAR_SERVICES" ]; then
  if [ "$OTHER_SERVICES" ]; then
    echo "Отмена. Этот порт занят другим сервисом."
    exit
  fi
  if [ "$FORCE" == "false" ]; then
    echo "Отмена. Этот порт занят одноимённым сервисом. Используйте флаг --force."
    exit
  fi
fi

# Сборка
yarn run build

# Упаковка в архив
if [ -f "./$ARCHIVE_NAME" ]; then
  if [ "$FORCE" == "true" ]; then
    echo "Удаление старого архива $ARCHIVE_NAME"
    rm "$ARCHIVE_NAME"
  else
    echo "Отмена. Архив уже существует. Используйте флаг --force чтобы пересоздать его."
    exit
  fi
fi
echo "Архивирование..."
zip -9 -r --exclude=*node_modules* \
          --exclude=*\.zip \
          --exclude=*\.sh \
          --exclude=*tsconfig\.json \
          --exclude="$SERVICE_NAME*" \
          --exclude=\./src/* \
  "$ARCHIVE_NAME" . > /dev/null

# Загрузка архива на сервер
echo "Загрузка архива на сервер..."
scp -P "$SSH_PORT" "./$ARCHIVE_NAME" "$SSH_USER@$SSH_HOST:~/services"

# Распаковка и установка зависимостей, запуск сервиса
ssh -p "$SSH_PORT" "$SSH_USER@$SSH_HOST" <<EOF
cd ~/services
if [ -d $SERVICE_NAME ]; then
  if [ "$FORCE" == "true" ]; then
    echo "Удаление старого кода сервиса";
    rm -rf $SERVICE_NAME
  else
    echo "Код сервиса уже существует";
    exit
  fi
fi
unzip $ARCHIVE_NAME -d $SERVICE_NAME > /dev/null
cd ~/services/$SERVICE_NAME
mv ./build/src/* ./
rm -rf ./build
echo "Установка зависимостей...";
yarn install

echo "Создание конфига сервиса"
cat "./service.service" | \
  sed s/\$\{SSH_USER\}/"$SSH_USER"/ | \
  sed s/\$\{SERVICE_NAME\}/"$SERVICE_NAME"/ \
  > "./$SERVICE_NAME.service"

echo "Копирование конфига в /etc/systemd/system/ и запуск сервиса"
if [ -f "/etc/systemd/system/$SERVICE_NAME.service" ]; then
  if [ "$FORCE" == "true" ]; then
    echo "Удаление старого сервиса";
    if [ "$SSH_PASS" == "" ]; then
      sudo systemctl stop "$SERVICE_NAME.service"
      sudo systemctl disable "$SERVICE_NAME.service"
      sudo rm /etc/systemd/system/"$SERVICE_NAME.service"
      sudo systemctl daemon-reload
      sudo systemctl reset-failed
    else
      echo '${SSH_PASS}' | sudo -S systemctl stop "$SERVICE_NAME.service"
      echo '${SSH_PASS}' | sudo -S systemctl disable "$SERVICE_NAME.service"
      echo '${SSH_PASS}' | sudo -S rm /etc/systemd/system/"$SERVICE_NAME.service"
      echo '${SSH_PASS}' | sudo -S systemctl daemon-reload
      echo '${SSH_PASS}' | sudo -S systemctl reset-failed
    fi
  else
    echo "Конфиг сервиса уже скопирован в /etc/systemd/system/. Создание сервиса отменено.";
    exit
  fi
fi;

echo "Копирование конфига в /etc/systemd/system/";
if [ "$SSH_PASS" == "" ]; then
  sudo cp "./$SERVICE_NAME.service" "/etc/systemd/system/$SERVICE_NAME.service"
  sudo systemctl enable "$SERVICE_NAME.service"
  sudo systemctl daemon-reload
  sudo systemctl start "$SERVICE_NAME.service"
  sudo systemctl status "$SERVICE_NAME.service"
else
  echo '${SSH_PASS}' | sudo -S cp "./$SERVICE_NAME.service" "/etc/systemd/system/$SERVICE_NAME.service"
  echo '${SSH_PASS}' | sudo -S systemctl enable "$SERVICE_NAME.service"
  echo '${SSH_PASS}' | sudo -S systemctl daemon-reload
  echo '${SSH_PASS}' | sudo -S systemctl start "$SERVICE_NAME.service"
  echo '${SSH_PASS}' | sudo -S systemctl status "$SERVICE_NAME.service"
fi
EOF

# Тестирование
ssh -p "$SSH_PORT" "$SSH_USER@$SSH_HOST" <<EOF
cd ~/services/$SERVICE_NAME
NODE_ENV=production node node_modules/.bin/mocha tests/
EOF

if [ ! $? -eq 0 ]; then
  echo "Есть ошибки при тестировании сервиса!"
  exit
else
  echo "Сервис протестирован. Можно пререключаться (checkout.sh)."
fi
