#!/usr/bin/env bash

# Активация сервиса (переключение nginx)

RED="\033[0;31m"
NC="\033[0m" # No Color

# Переход в папку backend (на случай если запускается из другой папки)
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd "$DIR"

# Чтение конфига
if [ -f "./config.sh" ]; then
  source "./config.sh"
else
  source "../config.sh"
fi

# Получение имени
PACKAGE_NAME=$(node <<EOF
try {
  const package = require('./package.json');
  console.log(package.name);
} catch (ignored) {}
EOF
)
if [[ ! "$PACKAGE_NAME" =~ ^[a-z0-9_-]+$ ]]; then
  echo "Отмена. C именем пакета что-то не так: $PACKAGE_NAME"
  exit
fi
echo "PACKAGE_NAME: $PACKAGE_NAME"

# Версия (порт)
VERSION=$1
WEB_SERVER_PORT=$VERSION
if [[ ! "$VERSION" =~ ^[0-9]+$ ]]; then
  echo "Отмена. C версией сервиса что-то не так: \"$VERSION\"."
  echo "Задайте её первым аргументом при вызове скрипта."
  exit
fi
echo "Версия: $VERSION"

# Имя сервиса
SERVICE_NAME="$PACKAGE_NAME-$VERSION"
echo "Полное имя сервиса: $SERVICE_NAME"

# Проверка существования сервиса и тестирование
ssh -p "$SSH_PORT" "$SSH_USER@$SSH_HOST" <<EOF
cd ~/services/
if [ ! -d $SERVICE_NAME ]; then
  echo "Отмена. Сервис не найден."
  exit 1
fi
cd ~/services/$SERVICE_NAME
NODE_ENV=production node node_modules/.bin/mocha tests/*
EOF
if [ ! $? -eq 0 ]; then
  echo "Отмена. Ошибка при тестировании сервиса."
  exit
fi

source "../shell_scripts/confirm.sh"
! confirm "Продолжить? [y/N]" && exit

ssh -p "$SSH_PORT" "$SSH_USER@$SSH_HOST" <<EOF
cd ~/services/$SERVICE_NAME
echo "Создание конфига nginx"
cat "./nginx-location.conf" | \
  sed s/\$\{WEB_SERVER_PORT\}/"$WEB_SERVER_PORT"/ \
  > "./$PACKAGE_NAME.conf"
echo "Переключаемся на сервис $SERVICE_NAME"
echo "WEB_SERVER_PORT: $WEB_SERVER_PORT"
echo "~~~"
cat "$PACKAGE_NAME.conf"
echo "~~~"
echo "Копирование конфига в $NGINX_DIR/$PACKAGE_NAME.conf"
if [ "$SSH_PASS" == "" ]; then
  sudo cp "./$PACKAGE_NAME.conf" "$NGINX_DIR/$PACKAGE_NAME.conf"
else
  echo '${SSH_PASS}' | sudo -S cp "./$PACKAGE_NAME.conf" "$NGINX_DIR/$PACKAGE_NAME.conf"
fi
echo "Перезапуск nginx"
if [ "$SSH_PASS" == "" ]; then
  sudo nginx -s reload
else
  echo '${SSH_PASS}' | sudo -S nginx -s reload
fi
echo "Готово!"
EOF
