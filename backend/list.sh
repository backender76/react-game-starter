#!/usr/bin/env bash

# Переход в папку backend (на случай если запускается из другой папки)
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd "$DIR"

# Чтение конфига
if [ -f "./config.sh" ]; then
  source "./config.sh"
else
  source "../config.sh"
fi

# Получение имени
PACKAGE_NAME=$(node <<EOF
try {
  const package = require('./package.json');
  console.log(package.name);
} catch (ignored) {}
EOF
)
echo "PACKAGE_NAME: $PACKAGE_NAME"

ssh -p "$SSH_PORT" "$SSH_USER@$SSH_HOST" <<EOF
echo "Список активных версий сервиса:"
systemctl list-unit-files | grep enabled | grep $PACKAGE_NAME
EOF
