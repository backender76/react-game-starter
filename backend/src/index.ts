import express from 'express';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import config from 'config';
// import mongoose from 'mongoose';

import router from './routers';
import {authMiddleware} from './middlewares/auth';

const {port} = config.get('webServer');

// const { url, db } = config.get('mongodb');
// mongoose.connect(`${url}/${db}`, {
//   useNewUrlParser: true,
//   useCreateIndex: true,
//   useFindAndModify: false,
//   useUnifiedTopology: true,
// }).then(() => {
//   console.log(`connect to ${url}/${db}`);
// });


const app = express();

app.disable('x-powered-by');

app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

app.use('/static', express.static('./public'));

app.use(bodyParser.json());
app.use(cookieParser());
app.use(authMiddleware);
app.use('/api', router);

app.listen(port, () => {
  console.log(`server listening on port ${port}`);
});
