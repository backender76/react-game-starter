import {Request, Response, NextFunction} from 'express';

export type ApiRequest = Request & {
  metrikaId: string;
};

export const authMiddleware = (req: ApiRequest, res: Response, next: NextFunction) => {
  req.metrikaId = String(req.cookies['_ym_uid']);
  next();
};
