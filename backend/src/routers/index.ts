import expressPromiseRouter from 'express-promise-router';
import * as exampleController from '../controllers/example';

const router = expressPromiseRouter();

router.post('/example', exampleController.example);

export default router;
