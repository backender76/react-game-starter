import { Request, Response } from 'express';

export const example = async (req: Request, res: Response) => {
  res.send({foo: 'bar'});
};
