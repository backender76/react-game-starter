React Game Starter
==================

Redux, Yandex Games SDK, Yandex Metrika

### Установка и настройка

1) Делаем форк

2) Клонируем форк

3) Устанавливаем зависимости `yarn install`

4) Создаём `config.sh` в корне проекта:

    ```shell script
    #!/usr/bin/env bash
    
    # SSH
    SSH_PORT="22"
    SSH_HOST="80.200.150.95"
    SSH_USER="deployer"
    SSH_PASS="" # Если deployer-у требуется ввод пароля для sudo

    # frontend test deploy
    WWW_PATH="/var/www/sitename.ru"

    # backend configs
    NGINX_DIR="/etc/nginx/sitename.ru-location"
    ```

5) Меняем по вкусу имя пакета в `frontend/package.json`. От него будет зависеть имя архива сборки. И имя пакета в `backend/package.json`, от него будет зависеть имя сервиса.

6) Инициализация git flow: `git flow`. Можно и без него, но перед релизной сборкой нужно помечать коммит в master тегом вида: `vN.N.N.N`, где N - натуральные числа.

7) Создаем конфиг для backend `backend/config/production.json`. Можно просто скопировать `default.json`.


### Сборка и деплой

1) Сборка и деплой фронтенда на тестовый сервер:

    ```shell script
    /bin/bash ./frontend/test-deploy.sh
    ```

2) Создание релизной сборки для каталога "Яндекс Игры"

    `/bin/bash  ./frontend/production-build.sh`
    
    Важные условия:
    
    - Сборка работает только на ветке master
    - Коммит должен быть помечен тегом соответствующим `^v[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$`
    - В рбочем каталоге не должно быть не сохранённых изменений

3) Деплой Backend-а

    Предполагается что один из хостов nginx имеет примерно такую настроку:
    
    ```text
    include /etc/nginx/site.ru-location/*.conf;
    ```
   
    Тоесть хост подключает файлы *.conf из дирректории `/etc/nginx/site.ru-location`. Именно туда будут закидываться конфиги backend-сервисов для nginx, обеспечивающие возможность обращаться к ним извне. Сами конфиги выглядят примерно так:
    
    ```text
    location /yandex-games/react-game-starter/ {
        proxy_pass http://127.0.0.1:${WEB_SERVER_PORT};
        proxy_set_header Host $host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Real-IP $remote_addr;
    }
    ```
    
    Шаблон такого конфига лежит в `backend/nginx-location.conf`. Перед первым деплоем нужно поменять в нём URL. А путь `/etc/nginx/site.ru-location` задать `config.sh` переменной `NGINX_DIR` и создать соответствующий каталог.

    - deploy
    
        Деплой `/bin/bash backend/deploy.sh`. Скрипт деплоя загружает код на сервер, создаёт и запускает сервис и тестирует его.
    
        Для правильной работы скрипта на сервере должен стоять Yarn. Либо можно переколхозить на package-lock.json. Инструкцию по установке Yarn можно найтив конце этого README.
        
    - checkout

        После успаешного деплоя нужно переключить Nginx на этот сервис, кроме случая когда сервис уже был активирован и деплой осуществлялся с флагом `--force`.

### Яндекс Метрика

Чтобы активировать метрику достаточно задать ID счётчика в `frontend/src/common.ts`.

```typescript
export const YANDEX_METRIKA_ID: string = '123456';
```

Пример использования:

```typescript
import {MetrikaCounter} from './frontend/src/components/YandexMetrika';

// Параметры визитов
MetrikaCounter
  .params('foo') // Преобразуется в {foo: 1}
  .params({[Date.now()]: 1})
  .params({
    'Ноль': 0, // Этот ключ будет удалён
    'Адын': 1,
    'Вложенность': {
      'Ноль': 0,
      'Адын': 1 // Этот ключ будет удалён
    }
  });
```

Ключи, значение которых равно нулю будут удалены перед выполнением запроса.


### Реклама (Fullscreen)

Подключается заворачиванием компоненты App в YandexGames.

```html
<!-- frontend/src/index.tsx -->
<YandexGames>
  <App />
</YandexGames>
```

Использование

```typescript
import {showFullscreenAdv} from './frontend/src/components/YandexGames';

showFullscreenAdv();
```

Предполагается что прошло достаточно времени чтобы SDK загрузился. Чтобы показать рекламу сразу необходимо подождать появления `YandexGames.sdk` реализовав метод подобный `waitSkdLoad`. Из коробки этого нет, т.к. кажется, что это не самая хорошая идея.

Настройки показа полноэкранных объявлений:

```typescript
// frontend/src/common.ts
export const YANDEX_GAMES_SDK_OPTIONS = {
  screen: {
    fullscreen: 'ontouchstart' in window,
  },
};
```


### Реклама (Баннер справа 240px на 100%)

Чтобы использовать блок нужно задать его ID в `frontend/src/common.ts`.

```typescript
export const YANDEX_RTB_ID: string = 'yandex_rtb_R-A-123456-2';
```

Там же есть переменные определяющие ширину экрана при которой активируется баннер и фоновый цвет баннера. Ширину баннера можно изменить через константу `RIGHT_BANNER_WIDTH`;

При использовании правого баннера компонента приложения (App) помещается внутри блока с`position: relative; width: calc(100% - ${RIGHT_BANNER_WIDTH}px);`. Таким образом, кнопочки которые должны быть справа, можно легко спозиционировать не боясь что они залезут под блок рекламы.


### Бэкенд

По умолчанию Backend (express app) стартует на порту **3001**. Номер порта задан в `backend/config/default.json`. Если изменить это значение, то нужно отредактировать строку `"proxy": "http://localhost:3001"` в **package.json**, чтобы в режиме разработки webpack правильно проксировал http-запросы.

Перед деплоем нужно создать продакшен конфиг `backend/config/production.json`. Путь к нему уже добавлен в `backend/.gitignore`.

Backend это не обязательная часть проекта. Для DEVELOP-окружения он нужен только чтобы отдавть `backend/public/fake-yandex-sdk.js`, а PRODUCTION-сборка вполне может обойтись без него.


### Установка Yarn на Debian 10

```shell script
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

sudo apt update && sudo apt install yarn
```

[Подробная инструкция](https://classic.yarnpkg.com/en/docs/install/#debian-stable)
