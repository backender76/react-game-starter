const { promises: fsp } = require('fs');
const path = require('path');

let assets = require('./build/asset-manifest');

assets = Object.values(assets.files)
    .filter(file => !/\.map$/.test(file))
    .map(file => "'" + file.replace(/^\.\//, '') + "'");

console.log(assets);

const version = Date.now();

const  content = `var version = 'v${version}';

self.addEventListener('install', function (event) {
    event.waitUntil(
        caches.open(version).then(function (cache) {
            return cache.addAll([
                'favicon.png',
                ${assets.join(',\n                ')}
            ]);
        })
    );
});

var cachePrefix = 'v';

self.addEventListener('activate', function (event) {
    event.waitUntil(
        caches.keys().then(function (keyList) {
            return Promise.all(keyList.map(function (key) {
                if (key.indexOf(cachePrefix) === 0 && key !== version) {
                    console.log('caches delete:', key);
                    return caches.delete(key);
                }
            }));
        })
    );
});

self.addEventListener('fetch', function (event) {
    var url = new URL(event.request.url);

    if (event.request.method === 'GET' && !/sw\\.js/.test(url.pathname) &&
        url.origin === this.location.origin
    ) {
        var cachedResponsePromise = caches.match(event.request, {ignoreSearch: true});
        event.respondWith(
            fetch(event.request).then(function (response) {
                if (response.ok) {
                    return caches.open(version).then(function (cache) {
                        var responseClone = response.clone();
                        return cache.put(event.request, responseClone).then(function () {
                            return response;
                        });
                    });
                } else {
                    return cachedResponsePromise;
                }
            }).catch(function () {
                return cachedResponsePromise;
            })
        );
    }
});
`;

console.log('sw version:', version);

fsp.writeFile(path.join(__dirname, 'build/sw.js'), content);
