export default function cellByIndex(index: number, gridSize: number[]): number[] {
  const i = index % gridSize[0];
  const j = Math.floor(index / gridSize[0]);
  return [i, j];
};
