function removeItem(array: any[], value: any): any[] {
    let result = [];
    for (let i = 0; i < array.length; i++) {
        if (array[i] !== value) {
            result.push(array[i]);
        }
    }
    return result;
}

export default removeItem;
