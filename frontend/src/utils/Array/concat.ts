function concat(array: any[]): any[] {
    if (array.length) {
        return Array.prototype.concat.apply([], array);
    }
    return array;
}

export default concat;
