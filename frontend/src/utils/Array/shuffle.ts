function shuffle(array: any[]): any[] {
    for (let i = array.length - 1; i > 0; i--) {
        let num = Math.floor(Math.random() * (i + 1));
        let d = array[num];
        array[num] = array[i];
        array[i] = d;
    }
    return array;
}

export default shuffle;
