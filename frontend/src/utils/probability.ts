function probability(percent: number = 50): boolean {
    return Math.random() <= percent / 100;
}

export default probability;
