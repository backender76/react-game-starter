import random from './random';

function randomItem(array: any[]): any {
    return array[random(0, array.length - 1)];
}

export default randomItem;
