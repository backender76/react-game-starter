import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './components/App';
import {store} from './store';
import {Provider} from 'react-redux';
import YandexMetrika from './components/YandexMetrika';
import {YANDEX_METRIKA_ID} from './common';
import YandexGames from './components/YandexGames';
import onAdBlockCallback from './components/onAdBlockCallback';

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <YandexMetrika counterId={YANDEX_METRIKA_ID}>
        <YandexGames onAdBlock={onAdBlockCallback}>
          <App />
        </YandexGames>
      </YandexMetrika>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

if (process.env.NODE_ENV === 'production') {
  if (navigator && navigator.serviceWorker) {
    const swPromise = navigator.serviceWorker.register('sw.js', {scope: './'});

    if (swPromise && swPromise.then) {
      swPromise.then(reg => console.log('sw register:', reg.scope));
    }
  }
}
