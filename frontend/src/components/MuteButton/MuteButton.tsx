import React from 'react'
import './MuteButton.css'
import sound from './sound.svg'
import soundOff from './sound-off.svg'

type ButtonProps = {
    pressed: boolean
    className?: string
    onClick?: Function
}

class MuteButton extends React.Component<ButtonProps, {}> {
    render(): JSX.Element {
        const {pressed, className} = this.props

        return (
            <button
                className={'Button MuteButton ' + (className ? className : '')}
                onClick={(e: React.MouseEvent) => this.onClick(e)}
            >
                <img
                    src={pressed ? soundOff : sound}
                    alt={pressed ? 'X' : 'O'}
                />
            </button>
        )
    }

    private onClick(event: React.MouseEvent): void {
        if (this.props.onClick) {
            this.props.onClick(event)
        }
    }
}

export default MuteButton
