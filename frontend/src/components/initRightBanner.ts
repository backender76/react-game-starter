import {RIGHT_BANNER_BACKGROUND, YANDEX_RTB_ID} from '../common';

const CHECK_AD_BLOCK_TIMEOUT: number = 2500;

export default (onAdBlock?: Function) => {
  const rightBanner = document.getElementById(YANDEX_RTB_ID);

  if (rightBanner) {
    rightBanner.style.background = RIGHT_BANNER_BACKGROUND;
    document.body.style.background = RIGHT_BANNER_BACKGROUND;
  }
  const timeoutId = setTimeout(() => {
    if (rightBanner) {
      const style = window.getComputedStyle(rightBanner);

      if (style && style['display'] && style['display'] === 'none') {
        if (onAdBlock) {
          onAdBlock();
        }
      }
    }
  }, CHECK_AD_BLOCK_TIMEOUT);

  const self = window;

  (function(w, d, n, s, t) {
    // @ts-ignore
    w[n] = w[n] || [];
    // @ts-ignore
    w[n].push(function() {
      // @ts-ignore
      Ya.Context.AdvManager.render({
        blockId: YANDEX_RTB_ID.replace('yandex_rtb_', ''),
        renderTo: YANDEX_RTB_ID,
        async: true,
        onRender: () => {
          clearTimeout(timeoutId);
        },
      })
    });
    // @ts-ignore
    t = d.getElementsByTagName('script')[0];
    // @ts-ignore
    s = d.createElement('script');
    // @ts-ignore
    s.type = 'text/javascript';
    // @ts-ignore
    s.src = '//an.yandex.ru/system/context.js';
    // @ts-ignore
    s.async = true;
    // @ts-ignore
    t.parentNode.insertBefore(s, t);
  })(self, self.document, 'yandexContextAsyncCallbacks');
};
