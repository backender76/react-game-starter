import React from 'react';

type Props = {
  counterId: string
};

export default class YandexMetrika extends React.Component<Props, any> {
  public static counterId: string;

  render():  React.ReactNode {
    return this.props.children;
  }

  componentDidMount(): void {
    const {counterId} = this.props;

    if (counterId) {
      initCounter(counterId);
      YandexMetrika.counterId = counterId;
    } else {
      if (process.env.NODE_ENV === 'development') {
        console.log('Не задан ID счётчика!');
      }
    }
  }
}

const initCounter = (counterId: string) => {
  (function(d, w, c) {
    // @ts-ignore
    (w[c] = w[c] || []).push(function() {
      try {
        // @ts-ignore
        w['yaCounter' + counterId] = new Ya.Metrika({
          id: counterId,
          clickmap: true,
          trackLinks: true,
          accurateTrackBounce: true,
        })
      } catch (e) {}
    });

    let n = d.getElementsByTagName('script')[0],
      s = d.createElement('script'),
      f = function() {
        // @ts-ignore
        n.parentNode.insertBefore(s, n);
      };
    s.type = 'text/javascript';
    s.async = true;
    s.src = 'https://mc.yandex.ru/metrika/watch.js';

    // @ts-ignore
    if (w.opera === '[object Opera]') {
      d.addEventListener('DOMContentLoaded', f, false);
    } else {
      f();
    }
  })(document, window, 'yandex_metrika_callbacks');
};

export class MetrikaCounter {
  private static paramsData: any = {};
  private static timeoutId: number = 0;

  public static params(params: object | string) {
    if (MetrikaCounter.timeoutId) {
      clearTimeout(MetrikaCounter.timeoutId);
    }
    if (typeof params === 'string') {
      MetrikaCounter.paramsData[params] = 1;
    } else {
      MetrikaCounter.paramsData = Object.assign({}, MetrikaCounter.paramsData, params);
    }
    setTimeout(MetrikaCounter.sendParams, 0);

    return this;
  }

  private static sendParams() {
    const {counterId} = YandexMetrika;
    const params = Object.assign({}, MetrikaCounter.paramsData);

    if (Object.keys(params).length) {
      removeNullValues(params);

      try {
        console.log('Параметры визита:', params);
        // @ts-ignore
        let counter = window['yaCounter' + counterId];
        counter.params(MetrikaCounter.paramsData);
      } catch (err) {
        console.error('Ошибка отправки параметров визита:', err.message);
      }
      MetrikaCounter.paramsData = {};
    }
  }
}

const removeNullValues = (params: any) => {
  for (let key in params) if (params.hasOwnProperty(key)) {
    if (params[key] === 0) {
      delete params[key];
    } else if (typeof params[key] === 'object') {
      removeNullValues(params[key]);
    }
  }
};
