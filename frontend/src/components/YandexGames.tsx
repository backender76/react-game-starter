import React from 'react';
import {
  FULLSCREEN_ADV_INTERVAL,
  isRightBanner,
  MIN_WIDTH_FOR_RIGHT_BANNER, RIGHT_BANNER_WIDTH,
  YANDEX_GAMES_SDK_OPTIONS,
  YANDEX_RTB_ID
} from '../common';
import styled from 'styled-components';
import './YandexGames.css';
import initRightBanner from './initRightBanner';

export const Banner = styled.div`
  position: absolute;
  right: 0;
  top: 0;
  width: ${RIGHT_BANNER_WIDTH}px;
  height: 100%;
  z-index: 100;
  display: none;

  @media (min-width: ${MIN_WIDTH_FOR_RIGHT_BANNER}px) {
    display: block;
  }
`;

export const AppWrapper = styled.div`
  position: relative;
  width: 100%;
  height: 100%;
  
  @media (min-width: ${MIN_WIDTH_FOR_RIGHT_BANNER}px) {
    width: calc(100% - ${RIGHT_BANNER_WIDTH}px);
  }
`;

type YandexGamesSdk = {
  init: Function;
};

type YandexGamesSdkInst = {
  adv: Adv
};

type Adv = {
  showFullscreenAdv: Function
};

export default class YandexGames extends React.Component<any, any> {
  public static sdk: YandexGamesSdkInst;

  render():  React.ReactNode {
    const {children} = this.props;

    return (
      <>
        {Boolean(YANDEX_RTB_ID) ? <AppWrapper>{children}</AppWrapper> : children}
        {Boolean(YANDEX_RTB_ID) && (
          <Banner id={YANDEX_RTB_ID}>
            <div className="rtb-info">Тут должна была быть реклама</div>
          </Banner>
        )}
      </>
    );
  }

  componentDidMount(): void {
    loadSdk((YaGames: YandexGamesSdk) => {
      YaGames
        .init(YANDEX_GAMES_SDK_OPTIONS)
        .then((sdk: YandexGamesSdkInst) => {
          YandexGames.sdk = sdk;
        });
    });

    if (isRightBanner() && YANDEX_RTB_ID) {
      initRightBanner(() => {
        if (this.props.onAdBlock) {
          this.props.onAdBlock();
        }
      });
    }
  }
}

const loadSdk = (callback: Function): void => {
  const target = document.getElementsByTagName('script')[0];

  if (target && target.parentNode) {
    const script = document.createElement('script');
    script.src =
      process.env.NODE_ENV === 'development'
        ? 'http://localhost:3001/static/fake-yandex-sdk.js'
        : 'https://yandex.ru/games/sdk/v2';
    script.async = true;
    target.parentNode.insertBefore(script, target);
    waitSkdLoad(callback);
  }
};

const waitSkdLoad = (callback: Function) => {
  // @ts-ignore
  if (typeof window['YaGames'] === 'undefined') {
    setTimeout(() => waitSkdLoad(callback), 100);
  } else {
    // @ts-ignore
    callback(window['YaGames']);
  }
};

let lastShowAdvTime = Date.now();

export const showFullscreenAdv = (onClose?: Function) => {
  if (YandexGames.sdk) {
    if (Date.now() - lastShowAdvTime > FULLSCREEN_ADV_INTERVAL) {
      YandexGames.sdk.adv.showFullscreenAdv({
        callbacks: {
          onClose: (wasShown: any) => {
            if (onClose) {
              onClose(wasShown);
            }
          }
        }
      });
      lastShowAdvTime = Date.now();
    } else {
      if (process.env.NODE_ENV === 'development') {
        let dt = Date.now() - lastShowAdvTime;
        let t = Math.ceil((FULLSCREEN_ADV_INTERVAL - dt) / 1000);
        console.log(`Реклама через: ${t} sec`);
      }
    }
  }
};
