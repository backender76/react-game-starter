import {RootState} from '../../store/reducer';

export const fooSelector = (store: RootState) => store.foo;
