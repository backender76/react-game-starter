import ReactDOM from 'react-dom'
import React from 'react'
import {Banner} from './YandexGames';
import {RIGHT_BANNER_BACKGROUND} from '../common';

/**
 * Код который выполняется когда AdBlock убивает баннер справа
 */
export default () => {
  const div = document.createElement('DIV');
  document.body.appendChild(div);

  ReactDOM.render(
    <Banner style={{background: RIGHT_BANNER_BACKGROUND}}>
      <span style={{color: 'white'}}>Пожалуйста, отключите AdBlock.</span>
    </Banner>,
    div
  )
};
