import React from 'react';
import logo from './logo.svg';
import './App.css';
import {connect, ConnectedProps} from 'react-redux';
import {fooSelector} from './selectors';
import {setFoo} from '../store/ac';
import {MetrikaCounter} from './YandexMetrika';
import {showFullscreenAdv} from './YandexGames';

const mapStoreToProps = (store: any, ownProps: any) => ({
  foo: fooSelector(store)
});

const mapDispatchToProps = (dispatch: any) => ({
  setFoo: (foo: number) => dispatch(setFoo(foo))
});

const connector = connect(mapStoreToProps, mapDispatchToProps);

type PropsFromRedux = ConnectedProps<typeof connector>

type Props = PropsFromRedux & {}

function App(props: Props) {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <p>
          <code>{JSON.stringify(props)}</code>
        </p>
        <p>
          <button onClick={() => {
            props.setFoo(2);
            MetrikaCounter
              .params('1')
              .params({[Date.now()]: 1})
              .params({
                'Ноль': 0,
                'Адын': 1,
                'Вложенность': {
                  'Ноль': 0,
                  'Адын': 1
                }
              });
            showFullscreenAdv();
          }}>setFoo(2)</button>
        </p>
      </header>
    </div>
  );
}

export default connector(App);
