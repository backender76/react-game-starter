import React from 'react'
import './Button.css'

type ButtonProps = {
    className?: string
    onClick?: Function
    size?: string
    background?: string
    disabled?: boolean
}

class Button extends React.Component<ButtonProps, {}> {
    render(): JSX.Element {
        const {className, size, background, children, disabled} = this.props;
        return (
            <button
                className={
                    'Button ' +
                    (className || '') +
                    (size ? ' Button_' + size : '') +
                    (disabled ? ' Button_disabled' : '')
                }
                onClick={(e: React.MouseEvent) => this.onClick(e)}
                style={background ? {background: background} : {}}
            >
                {children}
            </button>
        )
    }

    private onClick(event: React.MouseEvent): void {
        if (this.props.onClick) {
            this.props.onClick(event);
        }
    }
}

export default Button;
