import {combineReducers} from 'redux';
import {fooReducer} from './fooReducer';

export const rootReducer = combineReducers({
  foo: fooReducer,
});

export type RootState = ReturnType<typeof rootReducer>
