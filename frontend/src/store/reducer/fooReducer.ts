import {FOO_ACTION} from '../common';

const fooState: number = 1;

export const fooReducer = (foo: number = fooState, action: any) => {
  if (action.type === FOO_ACTION) {
    return action.value;
  }
  return foo;
};
