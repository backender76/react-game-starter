import {FOO_ACTION} from '../common';

export const setFoo = (value: number) => ({type: FOO_ACTION, value});
