import {createStore, applyMiddleware} from 'redux';
import {rootReducer} from './reducer';
import {logging} from './middlewares/logging';
import {localStorage} from './middlewares/localStorage';

const store = createStore(rootReducer, applyMiddleware(logging, localStorage));

if (process.env.NODE_ENV === 'development') {
  // @ts-ignore
  window.store = store;
}

export {store}
