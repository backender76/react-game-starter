export const CANVAS_SCALE =
    process.env.NODE_ENV === 'development' ? 2 : window.devicePixelRatio || 1;

class Canvas {
    public readonly canvas: HTMLCanvasElement;
    public readonly ctx: CanvasRenderingContext2D;
    public width: number;
    public height: number;

    constructor(width: number, height: number) {
        this.width = width;
        this.height = height;
        this.canvas = document.createElement('CANVAS') as HTMLCanvasElement;
        this.ctx = this.canvas.getContext('2d') as CanvasRenderingContext2D;
        this.ctx.scale(CANVAS_SCALE, CANVAS_SCALE);
        this.resize(width, height);
    }

    protected resize(width: number, height: number): void {
        this.width = width;
        this.height = height;
        this.canvas.width = width * CANVAS_SCALE;
        this.canvas.height = height * CANVAS_SCALE;
        this.canvas.style.width = width + 'px';
        this.canvas.style.height = height + 'px';
    }

    protected size() {
        return {width: this.canvas.width, height: this.canvas.height};
    }
}

export default Canvas;
