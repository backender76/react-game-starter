interface Sound {
    seed?: number
    volume?: number
    randomness?: number
    frequency?: number
    length?: number
    attack?: number
    slide?: number
    noise?: number
    modulation?: number
    modulationPhase?: number
}

// @ts-ignore
const AudioContext = window.AudioContext || window.webkitAudioContext;

class ZZFXLib {
    private readonly volume: number;
    private readonly randomness: number;
    private buffer: number[] | undefined;
    private r: number;
    private static context: AudioContext;

    constructor() {
        if (!ZZFXLib.context) {
            ZZFXLib.context = new AudioContext()
        }
        this.r = Date.now(); // starting value for random numbers
        this.buffer = undefined; // last sound buffer
        this.volume = 0.5; // master volume scale
        this.randomness = 0.1; // default frequency randomness
    }

    // play seeded sound with overrides applied
    public z(seed: number, soundOverrides: any) {
        // generate sound from seed
        let sound = this.Generate(seed);
        // copy overrides to sound
        for (let setting in soundOverrides) {
            if (soundOverrides.hasOwnProperty(setting)) {
                // @ts-ignore
                sound[setting] = soundOverrides[setting];
            }
        }
        // play sound
        return this.Z(
            // @ts-ignore-start
            sound['volume'],
            sound['randomness'],
            sound['frequency'],
            sound['length'],
            sound['attack'],
            sound['slide'],
            sound['noise'],
            sound['modulation'],
            sound['modulationPhase']
            // @ts-ignore-end
        )
    }

    // play sound with full parameter control
    private Z(
        volume: number,
        randomness: number,
        frequency: number,
        length: number = 1,
        attack: number = 0.1,
        slide: number = 0,
        noise: number = 0,
        modulation: number = 0,
        modulationPhase: number = 0
    ): AudioBufferSourceNode {
        // normalize parameters
        let sampleRate = 44100;
        volume *= this.volume;
        frequency *= (2 * Math.PI) / sampleRate;
        frequency *= 1 + randomness * (this.R() * 2 - 1);
        slide *= (Math.PI * 1e3) / sampleRate ** 2;
        length = length > 0 ? ((length > 10 ? 10 : length) * sampleRate) | 0 : 1;
        attack *= length | 0;
        modulation *= (2 * Math.PI) / sampleRate;
        modulationPhase *= Math.PI;

        // generate waveform
        let b = [],
            f = 0,
            fm = 0;

        for (let F = 0; F < length; ++F) {
            b[F] =
                volume * // volume
                Math.cos(
                    f *
                    frequency * // frequency
                        Math.cos(fm * modulation + modulationPhase)
                ) * // modulation
                (F < attack
                    ? F / attack // attack
                    : 1 - (F - attack) / (length - attack)); // decay
            f += 1 + noise * (this.R() * 2 - 1); // noise
            fm += 1 + noise * (this.R() * 2 - 1); // modulation noise
            frequency += slide // frequency slide
        }
        // play sound
        this.buffer = b;
        let B = ZZFXLib.context.createBuffer(1, b.length, sampleRate);
        let S = ZZFXLib.context.createBufferSource();
        B.getChannelData(0).set(b);
        S.buffer = B;
        S.connect(ZZFXLib.context.destination);
        S.start();
        return S;
    }

    // generate sound from seed
    private Generate(seed: number): Sound {
        let rSave = this.r; // save the seed
        this.r = seed; // set our seed
        for (let i = 9; i--; ) this.R(); // warm it up

        // generate parameters
        let sound: Sound = {};
        sound['seed'] = seed;
        sound['volume'] = 1;
        sound['randomness'] = seed ? this.randomness : 0;
        sound['frequency'] = seed ? (this.R() ** 2 * 2e3) | 0 : 220;
        sound['slide'] = seed ? parseFloat((this.R() ** 3 * 10).toFixed(1)) : 0;
        sound['length'] = seed ? parseFloat((0.1 + this.R()).toFixed(1)) : 1;
        sound['attack'] = seed ? parseFloat(this.R().toFixed(2)) : 0.1;
        sound['noise'] = seed ? parseFloat((this.R() ** 3 * 5).toFixed(1)) : 0;
        sound['modulation'] = seed
            ? parseFloat((this.R() ** 5 * 99).toFixed(1))
            : 0;
        sound['modulationPhase'] = seed ? parseFloat(this.R().toFixed(2)) : 0;

        this.r = rSave; // restore rand seed
        return sound;
    }

    // // get frequency of a musical note
    // private Note(rootNoteFrequency: number, semitoneOffset: number): number {
    //     return rootNoteFrequency * 2 ** (semitoneOffset / 12)
    // }

    // random seeded float
    private R(): number {
        this.r ^= this.r << 13;
        this.r ^= this.r >> 7;
        this.r ^= this.r << 17;
        return (Math.abs(this.r) % 1e9) / 1e9;
    }
}

// @ts-ignore
export default ZZFXLib
