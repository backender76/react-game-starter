interface Values {
    [state: string]: any
}

export default class Storage {
    private readonly key: string;
    private readonly initialState: Values;

    constructor(key: string, initialState: Values = {}) {
        this.key = key;
        this.initialState = initialState;
    }

    public get(): Values {
        let state = {};

        try {
            const json = localStorage.getItem(this.key);
            state = json ? JSON.parse(json) : {};
        } catch (e) {
            localStorage.removeItem(this.key);
        }
        state = {...this.initialState, ...state};

        return state;
    }

    public upd(state: Values): void {
        state = {...this.get(), ...state};
        this.set(state);
    }

    private set(state: Values): void {
        localStorage.setItem(this.key, JSON.stringify(state));
    }
}
