import ZZFX from './modules/ZZFXLib'

export const YANDEX_METRIKA_ID: string = '';

export const IS_TOUCH_DEVICE: boolean = 'ontouchstart' in window;

/** Настройки Yandex Games SDK */

export const YANDEX_GAMES_SDK_OPTIONS = {
  screen: {
    fullscreen: IS_TOUCH_DEVICE,
  },
};

// Частота показа рекламы
export const FULLSCREEN_ADV_INTERVAL: number = 3.5 * 60 * 1000; // ms

/** Настройки RTB-баннера */

export const YANDEX_RTB_ID: string = 'yandex_rtb_R-A-000000-2'; // example: 'yandex_rtb_R-A-000000-2'

// Минимальная ширина экрана для показа баннера справа
// Для touch-девайсов отключаем баннера ставя недостижимую ширину
export const MIN_WIDTH_FOR_RIGHT_BANNER: number = IS_TOUCH_DEVICE ? 10e6 : 1000;

export const RIGHT_BANNER_WIDTH: number = 240; // Ширина самого банера

export const isRightBanner = (): boolean => window.innerWidth >= MIN_WIDTH_FOR_RIGHT_BANNER;

export const RIGHT_BANNER_BACKGROUND: string = '#0f3443';

// Звуки

let globalMute = false;

export const globalMuteSound = (timeout: number = 3000) => {
  globalMute = true;
  setTimeout(() => (globalMute = false), timeout)
};

const playSound = (seed: number, volume: number = 0.2) => {
  if (!globalMute) {
    new ZZFX().z(seed, {volume})
  }
};

export const playClickSound = () => {
  playSound(40846)
};
