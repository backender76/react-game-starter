function AudioContextMock() {
    // @ts-ignore
    global.AudioContext = function() {
        // @ts-ignore
        this.createBuffer = function() {
            // @ts-ignore
            return new Buffer()
        };
        // @ts-ignore
        this.createBufferSource = function() {
            // @ts-ignore
            return new BufferSource()
        }
    };

    function Buffer() {}
    Buffer.prototype = {
        getChannelData: function() {
            // @ts-ignore
            return new ChannelData()
        },
    };

    function ChannelData() {}
    ChannelData.prototype = {
        set: function() {},
    };

    function BufferSource() {}
    BufferSource.prototype = {
        connect: function() {},
        start: function() {},
    }
}

export default AudioContextMock;
