#!/usr/bin/env bash

# Создание продакшен-сборки для Yandex Games

RED="\033[0;31m"
NC="\033[0m" # No Color

# Переход в папку frontend (на случай если запускается из другой папки)
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd "$DIR"

# Получением имени пакета
PACKAGE_NAME=$(node <<EOF
try {
  const package = require('./package.json');
  console.log(package.name);
} catch (ignored) {}
EOF
)
if [[ ! "$PACKAGE_NAME" =~ ^[a-z0-9_-]+$ ]]; then
  echo "${RED}Отмена. C именем пакета (поле name из package.json) что-то не так: \"$PACKAGE_NAME\"${NC}"
  echo "Имя должно соответствовать шаблону ^[a-z0-9_-]+$"
  exit
fi
echo "Имя проекта: $PACKAGE_NAME"

# Проверка текущей ветки
GIT_BRANCH_NAME=$(git rev-parse --abbrev-ref HEAD)

if [ "$GIT_BRANCH_NAME" != "master" ]; then
  echo -e "${RED}Отмена. Сборка допускается только из ветки master. Текущая ветка: ${GIT_BRANCH_NAME}${NC}"
  exit
fi

# Проверка наличия изменений в рабочем каталоге
GIT_STATUS=$(git status | grep frontend)

if [ "$GIT_STATUS" != "" ]; then
  echo -e "${RED}Отмена. Имеются не сохранённые изменения в рабочем каталоге.${NC}"
  echo "$GIT_STATUS"
  exit
fi

# Метка коммита
GIT_TAG=$(git tag -l --points-at HEAD)

if [[ ! "$GIT_TAG" =~ ^v[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
    echo "${RED}Отмена. C меткой коммита (git tag) что-то не так: \"$GIT_TAG\"${NC}"
    echo "Метка должна соответствовать шаблону ^v[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$"
    exit
fi

# Сборка проекта
if [ -d ./build ]; then
  rm -rf ./build
fi
yarn run build

# Имя архива
ARCHIVE_NAME="$PACKAGE_NAME-$GIT_TAG.zip"

# Упаковка в архив
if [ -f "./$ARCHIVE_NAME" ]; then
  echo "Архив с таким именем уже существует. Удаление..."
  rm "./$ARCHIVE_NAME"
fi
echo "Создание архива \"$ARCHIVE_NAME\"..."
cd ./build
zip -9 -r "../$ARCHIVE_NAME" * > /dev/null
cd ./..

echo "Готово!"
