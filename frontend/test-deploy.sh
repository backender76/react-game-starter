#!/usr/bin/env bash

# Деплой сборки frontend-а для тестов

# Переход в папку frontend (на случай если запускается из другой папки)
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd "$DIR"

# Чтение конфига
if [ -f "./config.sh" ]; then
  source "./config.sh"
else
  source "../config.sh"
fi

echo "Начало выгрузки frontend-сборки на сервер $SSH_HOST"
echo "Пользователь SSH: $SSH_USER"

# Получением имени пакета
POJECT_NAME=$(node <<EOF
const package = require('./package.json');
console.log(package.name);
EOF
)
echo "Имя проекта: $POJECT_NAME"

WWW_PATHNAME="$WWW_PATH/$POJECT_NAME"
echo "Путь на сервере: $WWW_PATHNAME"

source "../shell_scripts/confirm.sh"
! confirm "Продолжить?" && exit

# Сборка проекта
if [ -d ./build ]; then
  rm -rf ./build
fi
yarn run build

ARCHIVE_NAME="$POJECT_NAME.zip"

# Упаковка в архив
if [ -f "./$ARCHIVE_NAME" ]; then
  echo "Архив уже создан. Удаление архива..."
  rm "./$ARCHIVE_NAME"
fi
echo "Создание архива..."
cd ./build
zip -9 -r "../$ARCHIVE_NAME" * > /dev/null
cd ./..

# Удаление старой версии
if ssh -p "$SSH_PORT" "$SSH_USER@$SSH_HOST" stat "~/$ARCHIVE_NAME" \> /dev/null 2\>\&1; then
  ssh -p "$SSH_PORT" "$SSH_USER@$SSH_HOST" rm "~/$ARCHIVE_NAME"
fi

if ssh -p "$SSH_PORT" "$SSH_USER@$SSH_HOST" stat "$WWW_PATHNAME" \> /dev/null 2\>\&1; then
  ssh -p "$SSH_PORT" "$SSH_USER@$SSH_HOST" rm -rf "$WWW_PATHNAME"
fi

# Загрузка новой версии
scp -P "$SSH_PORT" "./$ARCHIVE_NAME" "$SSH_USER@$SSH_HOST:~"
ssh -p "$SSH_PORT" "$SSH_USER@$SSH_HOST" unzip "~/$ARCHIVE_NAME" -d "$WWW_PATHNAME" > /dev/null

echo "https://papergame.ru/$POJECT_NAME/index.html"
